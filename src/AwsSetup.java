/**
 * @author Smitha Bangalore Naresh && Ajay Subramanya
 * @date 01/29/2016
 * @info interfaces with AWS EC2, S3 and EMR to instantiate EC2, upload files to S3 and 
 * 		 finally create a cluster to run the supplied JAR  
 */

import java.io.File;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;

public class AwsSetup {
	
	public static final String BUCKETNAME = "javasdktestbucketsmithajay";
	
	// args list : pathOfCodeJar, pathOfInputFile, 	
	
	/**
	 * 
	 * @param args : inputFile, jar, keyPair
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		try{
			
			// ------------------------------------ EC2 ------------------------------------//
			Region usWest2 = Region.getRegion(Regions.US_WEST_2);
			EC2 ec2Client = new EC2();

			
			System.out.println("creating ec2 instance ... ");
			
			// create EC2 instance
			String instanceId = ec2Client.createInstance();

			System.out.println("starting ec2 instance ... ");
			
			// start the EC2 instance
			ec2Client.startEC2(instanceId);
			
			// ------------------------------------ EC2 ------------------------------------//
			
			
			// ------------------------------------ S3 ------------------------------------//
			
			S3 s3Client = new S3();
			
			// create s3 bucket 
			System.out.println("creating s3 bucket ... "+BUCKETNAME);
			
			s3Client.createBucket(usWest2, BUCKETNAME);
			
			System.out.println("uploading JAR  ... ");
		
			//create a folder to upload JAR
			s3Client.createFolder(BUCKETNAME, "code");
			// upload jar to the created folder 
			s3Client.uploadFile(BUCKETNAME+"/code", "a1_distribution_job.jar", new File(args[1].toString()));
			
			System.out.println("uploading input files  ... might take a while");
			
			s3Client.uploadDirectory(BUCKETNAME, "input", new File(args[0].toString()));
			
			// ------------------------------------ S3 ------------------------------------//
			
			// ------------------------------------ EMR ------------------------------------//
			
			EMR emrClient = new EMR();

			System.out.println("creating EMR cluster  ... ");
			
			// create EMR cluster with the correct parameters 
			String inputFile = "s3://"+ BUCKETNAME +"/input";
			String jarPath = "s3://"+ BUCKETNAME +"/code/newimplementation1.jar";
			String outputPath = "s3://"+ BUCKETNAME +"/output";
			
			emrClient.createCluster("A1-Distribution", jarPath, "AvgPricePerMonthCal", inputFile, outputPath);
			
			// ------------------------------------ EMR ------------------------------------//

		}catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it "
                    + "to Amazon , but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with , "
                    + "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
	}
}
