import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.amazonaws.services.s3.transfer.TransferManager;

public class S3 {
	private AWSCredentials credentials = new ProfileCredentialsProvider().getCredentials();
	private AmazonS3Client s3 = new AmazonS3Client(credentials);
	private static final String SUFFIX = "/";

	public void createBucket(Region region, String bucketName) {

		s3.setRegion(region);
		s3.createBucket(bucketName);
	}

	/**
	 * @author https://javatutorial.net/java-s3-example
	 * @param bucketName
	 * @param folderName
	 */
	public void createFolder(String bucketName, String folderName) {
		// create meta-data for your folder and set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folderName + SUFFIX, emptyContent,
				metadata);
		// send request to S3 to create folder
		s3.putObject(putObjectRequest);
	}

	public void listBuckets() {
		for (Bucket bucket : s3.listBuckets()) {
			System.out.println(bucket.getName());
		}
	}

	public void uploadFile(String bucketName, String key, File aFile) {
		s3.putObject(new PutObjectRequest(bucketName, key, aFile));
	}

	public void uploadDirectory(String bucketName, String dirNameOnS3, File inputFilePath) {
		TransferManager tx = new TransferManager(credentials);
		MultipleFileUpload myUpload = tx.uploadDirectory(bucketName, dirNameOnS3,
				inputFilePath, true);

		// Or you can block the current thread and wait for your transfer to
		// to complete. If the transfer fails, this method will throw an
		// AmazonClientException or AmazonServiceException detailing the reason.
		try {
			myUpload.waitForCompletion();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// After the upload is complete, call shutdownNow to release the
		// resources.
		tx.shutdownNow();
	}

	public void listObjsInBucket(String bucketName) {
		// List object in bucket.
		ObjectListing objectListing = s3
				.listObjects(new ListObjectsRequest().withBucketName(bucketName).withPrefix("My"));
		for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
			System.out.println(objectSummary.getKey() + "  " + "(size = " + objectSummary.getSize() + ")");
		}
	}

	public void downloadAndReadFile(String bucketName, String key) {
		// Download and read an object (text file).
		S3Object object = s3.getObject(new GetObjectRequest(bucketName, key));
		BufferedReader reader = new BufferedReader(new InputStreamReader(object.getObjectContent()));
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void deleteFile(String bucketName, String key) {
		s3.deleteObject(bucketName, key);
	}

	public void deleteBucket(String bucketName) {
		s3.deleteBucket(bucketName);
	}

}
