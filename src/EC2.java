import java.util.Arrays;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;


public class EC2 {
	
	private AWSCredentials credentials = new ProfileCredentialsProvider().getCredentials();
	private AmazonEC2Client amazonEC2Client = new AmazonEC2Client(credentials);
	
	public String createInstance(){	
		amazonEC2Client.setEndpoint("ec2.us-west-2.amazonaws.com");
		RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
		runInstancesRequest.withImageId("ami-f0091d91").withInstanceType("t2.micro").withMinCount(1).withMaxCount(1)
				.withKeyName("subramanya.a_key_pair");
		RunInstancesResult runInstancesResult = amazonEC2Client.runInstances(runInstancesRequest);
		final String instanceId = runInstancesResult.getReservation().getInstances().get(0).getInstanceId();
		System.out.println("Id: " + instanceId);
		return instanceId;
	}
	
	public void startEC2(String instanceId) {
		StartInstancesRequest startInstancesRequest = new StartInstancesRequest().withInstanceIds(instanceId);
		StartInstancesResult startInstancesResult = amazonEC2Client.startInstances(startInstancesRequest);
		System.out.println("Start result : " + startInstancesResult.toString());
	}
	
	public void stopEC2(String instanceId){
		StopInstancesRequest stopInstancesRequest = new StopInstancesRequest(
				Arrays.asList(new String[] { instanceId }));
		StopInstancesResult stopInstancesResult = amazonEC2Client.stopInstances(stopInstancesRequest);
		System.out.println("Start result : " + stopInstancesResult.toString());
	}

}
