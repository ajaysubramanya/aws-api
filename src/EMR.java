import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.AddJobFlowStepsRequest;
import com.amazonaws.services.elasticmapreduce.model.AddJobFlowStepsResult;
import com.amazonaws.services.elasticmapreduce.model.HadoopJarStepConfig;
import com.amazonaws.services.elasticmapreduce.model.JobFlowInstancesConfig;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowRequest;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowResult;
import com.amazonaws.services.elasticmapreduce.model.StepConfig;

public class EMR {
	private AWSCredentials credentials = new ProfileCredentialsProvider().getCredentials();
	private AmazonElasticMapReduce client = new AmazonElasticMapReduceClient(credentials);
	
	public void createCluster(String clusterName, String jarPath, String mainClass, String inputPath,
			String outputPath) {
		client.setEndpoint("http://elasticmapreduce.us-west-2.amazonaws.com"); 
		HadoopJarStepConfig hadoopConfig = new HadoopJarStepConfig().withJar(jarPath).withMainClass(mainClass)
				.withArgs(inputPath, outputPath);

		StepConfig customStep = new StepConfig("Step One", hadoopConfig);
		

		RunJobFlowRequest request = new RunJobFlowRequest()
				.withName(clusterName).withReleaseLabel("emr-4.3.0")
				.withSteps(customStep)
				.withLogUri("s3://javasdktestbucketsmithajay/logs")
				.withServiceRole("EMR_DefaultRole")
				.withJobFlowRole("EMR_EC2_DefaultRole")
				.withInstances(new JobFlowInstancesConfig().withKeepJobFlowAliveWhenNoSteps(true)
				.withEc2KeyName("subramanya.a_key_pair").withInstanceCount(3).withKeepJobFlowAliveWhenNoSteps(true)
				.withMasterInstanceType("m3.xlarge").withSlaveInstanceType("m3.xlarge"));
		RunJobFlowResult result = client.runJobFlow(request);
		System.out.println("jobFlowId: " + result.getJobFlowId());
		
		
		// Add step to running cluster.
		HadoopJarStepConfig hadoopConfig2 = new HadoopJarStepConfig()
		.withJar(jarPath)
		.withMainClass(mainClass)
		.withArgs(inputPath, outputPath);
		StepConfig customStep2 = new StepConfig("Step Two", hadoopConfig2);
		
		AddJobFlowStepsRequest request2 = new AddJobFlowStepsRequest()
		            	.withJobFlowId(result.getJobFlowId())
		            	.withSteps(customStep2);
		AddJobFlowStepsResult result2 = client.addJobFlowSteps(request2);
		System.out.println("Step Id's : "+result2.getStepIds());
		System.out.println("cluster running ...");

	}
}
